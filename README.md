# IMPORTANT: This project has moved to Codeberg: https://codeberg.org/fswb/kindmind

[![Awesome Humane Tech](https://raw.githubusercontent.com/humanetech-community/awesome-humane-tech/main/humane-tech-badge.svg?sanitize=true)](https://github.com/humanetech-community/awesome-humane-tech)
[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/mindfulness-at-the-computer/community)
[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
[![pipeline status](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/badges/master/pipeline.svg)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/commits/master)
[![Latest Release](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/badges/release.svg)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/releases)
[![PyPI downloads last week](https://img.shields.io/pypi/dw/mindfulness-at-the-computer)](https://pypistats.org/packages/mindfulness-at-the-computer)
[![SourceForge downloads last week](https://img.shields.io/sourceforge/dw/mindfulness-at-the-computer.svg)](https://sourceforge.net/projects/mindfulness-at-the-computer/files/stats/timeline)
<!--
[![coverage report](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/badges/master/coverage.svg)](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/-/commits/master)
-->

# Mindfulness at the Computer

This application helps you to be mindful while using at the computer, and helps you concentrate on breathing in and out when you need a breathing pause.

**The [application website](https://mindfulness-at-the-computer.gitlab.io) has more information** (including screenshots and downloads).

Social: [**Gitter chat**](https://gitter.im/mindfulness-at-the-computer/community)

License: GPLv3 - a *free software* (free as in freedom) license, meaning that you can share, change and distribute the application.

The application is written in Python and uses Qt/PySide for the front-end.

The project has been featured in the [PyCoders Weekly](https://pycoders.com/) newsletter and is listed in [Awesome Humane Tech](https://github.com/humanetech-community/awesome-humane-tech)

Project goals:
* Providing a friendly environment for people new to software and development to contribute and improve their skills
* Promoting [free/libre software](https://www.gnu.org/philosophy/free-sw.en.html) (free as in freedom!) - and helping people see free software in the context of human rights

Software goals:
* Increasing mindfulness of breathing, body, and posture
* Helping people become more aware of (and kind to) their bodies when using the computer for long periods of time

## Installation and setup

Install with pip `pip install mindfulness-at-the-computer` or [**download the pre-built package**](https://sourceforge.net/projects/mindfulness-at-the-computer/files/latest/download) from sourceforge. (For details about installation and setup, please see [the website](https://mindfulness-at-the-computer.gitlab.io/installation/))

## What is Mindfulness?

https://mindfulness-at-the-computer.gitlab.io/mindfulness/

## Contributing to the project

*This project is beginner-friendly:* You can ask for help in the Gitter chat room and we will try to help you. Also we try to provide documentation useful for newcomers.

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for more information
