
# Technical Overview

The only external dependency for the software itself is PySide6, which is a Python binding for Qt

* [Qt docs](http://doc.qt.io/qt-5/)
* [online communities](https://wiki.qt.io/Online_Communities)

PyInstaller - [docs](http://pyinstaller.readthedocs.io/en/stable/)

`pytest` is used for auto-testing, with the `pytest-qt` plugin

What a typical application flow might look like:
1. The user interacts with a component in the application
1. The Qt/PySide component sends a `QtCore.Signal`
1. The application catches the signal (through the `connect`ed handler function)
   * Handler functions are named with the prefix `on_`
1. The handler function checks the `updating_gui_bool` variable and exits if `True`
   * Some of Qt's signals are fired only at user interaction, but often they are also fired at a programmatic change, which can happen in our `update_gui` functions. To avoid infinite loops we set a `updating_gui_bool` when needed
1. The handler function updates the application state (can be found in `state.py`)


## Code Conventions

These things differ from PEP8 or standard Python recommendations:
* one class per file (plus minor supporting classes)
* 100 chars max per line

### Variable naming

Suffixes:
* `_[type]`, for example `_int` --- this helps us be aware of type
  * For Qt the suffixes start with a `q`, for example `qpb` is short for `qPushButton`
* `_l[x]`, or `_wt[x]` where `wt`/`l` stands for widget/layout, and `[x]` is a number which describes the level of the widget or layout
  * This helps us understand the structure of the application in places where there are nested layouts

***

Also see the [technical research](https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer/wikis/Tech-Research
) page on the wiki
