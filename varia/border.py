"""
Border:
self.setStyleSheet(f"background-color: {matc.shared.BLACK_COLOR}; border: 8px solid {matc.shared.GRAY_COLOR}; border-radius: {CORNER_RADIUS+0}")
Reference: https://doc.qt.io/qt-5/stylesheet-reference.html#border-prop
Reference: https://doc.qt.io/qt-5/qframe.html#frameWidth-prop
"""
