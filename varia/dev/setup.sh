#!/bin/bash

echo "Setting hooksPath (to a directory that is verion-controlled)"
git config core.hooksPath ./varia/dev/hooks
# -inspiration: https://rjzaworski.com/2018/01/keeping-git-hooks-in-sync
