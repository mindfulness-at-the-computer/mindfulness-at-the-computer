import logging
import os
import shutil
import sys
import tempfile
from string import Template

from PySide6 import QtCore

import matc.shared

"""

https://pyinstaller.org/en/stable/runtime-information.html#using-file



https://doc.qt.io/qt-6/qstandardpaths.html


"""

LINUX_SHORTCUT_TEMPLATE_FILE_NAME = "mindfulness-at-the-computer[template].desktop"
LINUX_SHORTCUT_FILE_NAME = "mindfulness-at-the-computer.desktop"
WINDOWS_SHORTCUT_FILE_NAME = "mindfulness-at-the-computer.lnk"
WINDOWS_EXECUTABLE_FILE_NAME = "mindfulness-at-the-computer.exe"
LINUX_EXECUTABLE_FILE_NAME = "mindfulness-at-the-computer"


def create_shortcut_linux(i_target_dir_path: str):
    # ##### Assumes using pip installation, since local bin file #####

    os.makedirs(i_target_dir_path, exist_ok=True)

    read_icon_path = matc.shared.get_app_icon_path()
    write_icon_path = matc.shared.get_config_path(matc.shared.APPLICATION_ICON_FILE_NAME)
    logging.debug(f"Copying {read_icon_path} to {write_icon_path}")
    shutil.copy(read_icon_path, write_icon_path)

    template_desktop_file_path: str = matc.shared.get_res_path(LINUX_SHORTCUT_TEMPLATE_FILE_NAME)
    with open(template_desktop_file_path, "r") as f:
        template_content_str = f.read()
    user_home_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.HomeLocation)[0]

    matc_package_dir: str = os.path.dirname(os.path.abspath(__file__))
    pyinstaller_base_dir: str = os.path.dirname(matc_package_dir)

    run_method = matc.shared.get_appl_run_method()
    if run_method == matc.shared.ApplRunMethod.from_source:
        exec_file_path = os.path.join(user_home_dir, ".local", "bin",
            LINUX_EXECUTABLE_FILE_NAME)
    elif run_method == matc.shared.ApplRunMethod.from_pyinstaller_bundle:
        exe_file_path = os.path.join(pyinstaller_base_dir, LINUX_EXECUTABLE_FILE_NAME)
    else:
        raise Exception("Case not covered")

    template = Template(template_content_str)
    output_desktop_file_contents: str = template.substitute(
        exec=exec_file_path, icon=write_icon_path)
    with tempfile.TemporaryDirectory() as tmp_dir_path:
        logging.debug(f"{tmp_dir_path=}")
        output_desktop_file_path = os.path.join(tmp_dir_path, LINUX_SHORTCUT_FILE_NAME)
        with open(output_desktop_file_path, "w+") as output_file:
            output_file.write(output_desktop_file_contents)
        os.chmod(output_desktop_file_path, 0o744)
        # Please note that the lines above must be within the TemporaryDirectory context

        logging.debug(f"Copying {output_desktop_file_path} to {i_target_dir_path}")
        shutil.copy2(output_desktop_file_path, i_target_dir_path)


def create_shortcut_windows(i_target_dir_path: str):
    # ##### Assumes using from_pyinstaller_bundle package, since .exe file #####

    os.makedirs(i_target_dir_path, exist_ok=True)

    matc_package_dir: str = os.path.dirname(os.path.abspath(__file__))
    pyinstaller_base_dir: str = os.path.dirname(matc_package_dir)

    python_install_dir = os.path.dirname(sys.executable)
    python_scripts_dir = os.path.join(python_install_dir, "Scripts")
    # -this will start the application through a console window
    # -Ex: C:\Python39\Scripts\mindfulness-at-the-computer.exe

    run_method = matc.shared.get_appl_run_method()
    if run_method == matc.shared.ApplRunMethod.from_source:
        exe_file_path = os.path.join(python_scripts_dir, WINDOWS_EXECUTABLE_FILE_NAME)
    elif run_method == matc.shared.ApplRunMethod.from_pyinstaller_bundle:
        exe_file_path = os.path.join(pyinstaller_base_dir, WINDOWS_EXECUTABLE_FILE_NAME)
    else:
        raise Exception("Case not covered")

    exe_file_qf = QtCore.QFile(exe_file_path)
    logging.debug(f"{exe_file_path=}")

    link_path = os.path.join(i_target_dir_path, WINDOWS_SHORTCUT_FILE_NAME)
    exe_file_qf.link(link_path)
    logging.debug(f"{link_path=}")


def get_os_applications_path(i_file_name: str = "") -> str:
    user_appl_path = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.ApplicationsLocation)[0]
    if i_file_name:
        user_appl_path = os.path.join(user_appl_path, i_file_name)
    return user_appl_path


def get_linux_os_autostart_path(i_file_name: str = "") -> str:
    user_config_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.ConfigLocation)[0]
    user_autostart_path = os.path.join(user_config_dir, "autostart")
    if i_file_name:
        user_autostart_path = os.path.join(user_autostart_path, i_file_name)
    return user_autostart_path


def get_windows_os_autostart_path(i_file_name: str = "") -> str:
    user_applications_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.ApplicationsLocation)[0]
    user_autostart_path = os.path.join(user_applications_dir, "Startup")
    if i_file_name:
        user_autostart_path = os.path.join(user_autostart_path, i_file_name)
    return user_autostart_path


def get_os_desktop_path(i_file_name: str = "") -> str:
    user_desktop_path = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.DesktopLocation)[0]
    if i_file_name:
        user_desktop_path = os.path.join(user_desktop_path, i_file_name)
    return user_desktop_path


def has_shortcut_windows(i_shortcut_dir_path) -> bool:
    if not os.path.isdir(i_shortcut_dir_path):
        return False
    shortcut_file_path: str = os.path.join(i_shortcut_dir_path, WINDOWS_SHORTCUT_FILE_NAME)
    if os.path.isfile(shortcut_file_path):
        return True
    return False


def has_appl_menu_shortcut_windows() -> bool:
    return has_shortcut_windows(get_os_applications_path())


def has_autostart_shortcut_windows() -> bool:
    return has_shortcut_windows(get_windows_os_autostart_path())


def has_desktop_shortcut_windows() -> bool:
    return has_shortcut_windows(get_os_desktop_path())


def has_shortcut_linux(i_shortcut_dir_path) -> bool:
    if not os.path.isdir(i_shortcut_dir_path):
        return False
    shortcut_file_path: str = os.path.join(i_shortcut_dir_path, LINUX_SHORTCUT_FILE_NAME)
    if os.path.isfile(shortcut_file_path):
        return True
    return False


def has_appl_menu_shortcut_linux() -> bool:
    return has_shortcut_linux(get_os_applications_path())


def has_autostart_shortcut_linux() -> bool:
    return has_shortcut_linux(get_linux_os_autostart_path())


def has_desktop_shortcut_linux() -> bool:
    return has_shortcut_linux(get_os_desktop_path())


def del_appl_menu_shortcut_windows():
    if not has_appl_menu_shortcut_windows():
        logging.warning("There is no shortcut in the application menu system, so cannot remove")
        return
    shortcut_file_path: str = get_os_applications_path(WINDOWS_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Application menu system shortcut removed")


def del_autostart_shortcut_windows():
    if not has_autostart_shortcut_windows():
        logging.warning("There is no shortcut in autostart, so cannot remove")
        return
    shortcut_file_path: str = get_windows_os_autostart_path(WINDOWS_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Autostart shortcut removed")


def del_desktop_shortcut_windows():
    if not has_desktop_shortcut_windows():
        logging.warning("There is no shortcut on the desktop, so cannot remove")
        return
    shortcut_file_path: str = get_os_desktop_path(WINDOWS_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Desktop shortcut removed")


def del_appl_menu_shortcut_linux():
    if not has_appl_menu_shortcut_linux():
        logging.warning("There is no shortcut in the application menu system, so cannot remove")
        return
    shortcut_file_path: str = get_os_applications_path(LINUX_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Application menu system shortcut removed")


def del_autostart_shortcut_linux():
    if not has_autostart_shortcut_linux():
        logging.warning("There is no shortcut in autostart, so cannot remove")
        return
    shortcut_file_path: str = get_linux_os_autostart_path(LINUX_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Autostart shortcut removed")


def del_desktop_shortcut_linux():
    if not has_desktop_shortcut_linux():
        logging.warning("There is no shortcut on the desktop, so cannot remove")
        return
    shortcut_file_path: str = get_os_desktop_path(LINUX_SHORTCUT_FILE_NAME)
    os.remove(shortcut_file_path)
    logging.info("Desktop shortcut removed")


def add_appl_menu_shortcut_windows():
    if has_appl_menu_shortcut_windows():
        logging.warning("There is already a shortcut in the application menu system")
        return
    create_shortcut_windows(get_os_applications_path())
    logging.info("Application menu shortcut added")


def add_autostart_shortcut_windows():
    if has_autostart_shortcut_windows():
        logging.warning("The application shortcut is already in autostart")
        return
    create_shortcut_windows(get_windows_os_autostart_path())
    logging.info("Application shortcut added to autostart")


def add_desktop_shortcut_windows():
    if has_desktop_shortcut_windows():
        logging.warning("There is already a shortcut on the desktop")
        return
    create_shortcut_windows(get_os_desktop_path())
    logging.info("Desktop shortcut added")


def has_appl_menu_shortcut() -> bool:
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        return has_appl_menu_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        return has_appl_menu_shortcut_windows()
    else:
        raise Exception("Case not covered")


def has_autostart_shortcut() -> bool:
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        return has_autostart_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        return has_autostart_shortcut_windows()
    else:
        raise Exception("Case not covered")


def has_desktop_shortcut() -> bool:
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        return has_desktop_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        return has_desktop_shortcut_windows()
    else:
        raise Exception("Case not covered")


def del_desktop_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        del_desktop_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        del_desktop_shortcut_windows()
    else:
        raise Exception("Case not covered")


def add_desktop_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        add_desktop_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        add_desktop_shortcut_windows()
    else:
        raise Exception("Case not covered")


def add_appl_menu_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        add_appl_menu_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        add_appl_menu_shortcut_windows()
    else:
        raise Exception("Case not covered")


def del_appl_menu_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        del_appl_menu_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        del_appl_menu_shortcut_windows()
    else:
        raise Exception("Case not covered")


def del_autostart_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        del_autostart_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        del_autostart_shortcut_windows()
    else:
        raise Exception("Case not covered")


def add_autostart_shortcut():
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        add_autostart_shortcut_linux()
    elif platform == matc.shared.Platform.windows:
        add_autostart_shortcut_windows()
    else:
        raise Exception("Case not covered")


def add_appl_menu_shortcut_linux():
    if has_appl_menu_shortcut_linux():
        logging.warning("There is already a shortcut in the application menu system")
        return
    create_shortcut_linux(get_os_applications_path())
    logging.info("Application menu shortcut added")


def add_autostart_shortcut_linux():
    if has_autostart_shortcut_linux():
        logging.warning("The application shortcut is already in autostart")
        return
    create_shortcut_linux(get_linux_os_autostart_path())
    logging.info("Application shortcut added to autostart")


def add_desktop_shortcut_linux():
    if has_desktop_shortcut_linux():
        logging.warning("There is already a shortcut on the desktop")
        return
    create_shortcut_linux(get_os_desktop_path())
    logging.info("Desktop shortcut added")


def do_extra_setup_gnu_linux():
    """
    This function is called automatically during pip install. It creates a .desktop file and
    copies it to these dirs:
    * The user applications dir - so that the user can see the application shortcut in the menu
    system used by the
    desktop environment she is using
    * The autostart dir - so that the application is automatically launched on startup

    ### Menu .desktop files
    On Linux-based systems menu .desktop files are locally stored in
    ~/.local/share/applications (globally in /usr/share/applications)

    ### Autostart dir
    > $XDG_CONFIG_HOME defines the base directory relative to which user-specific configuration
    files should be stored.
    > If $XDG_CONFIG_HOME is either not set or empty, a default equal to $HOME/.config should be
    used.

    Based on the info above this is the default location: .desktop file in ~/.config/autostart

    Please note:
    * Only gnu/linux systems can run this extra setup file at the moment
    * Printouts are not written to the terminal unless the user has added the verbose flag at the
    end:
      `pip3 install -e . -v`

    There is no way to call a file at uninstall, but we could - in this script - create a text
    file with a list of the
    files that we have installed and therefore want to remove. And then have a simple script file
    which removes these
    files. One way to do this is described here: https://gist.github.com/myusuf3/933625 (I don't
    think we need to use
    sudo though)

    References:
    * Freedesktop spec:
      * https://www.freedesktop.org/wiki/Specifications/autostart-spec/
      * https://specifications.freedesktop.org/autostart-spec/autostart-spec-latest.html
    * https://doc.qt.io/qt-5/qstandardpaths.html#StandardLocation-enum
    """

    if matc.shared.get_platform() != matc.shared.Platform.gnu_linux:
        logging.debug("Only gnu/linux systems can run this extra setup at the moment")
        return

    # from PySide6 import QtCore

    print("====Running extra setup python script extra_setup.py====")
    user_home_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.HomeLocation)[0]
    user_config_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.ConfigLocation)[0]
    user_applications_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.ApplicationsLocation)[0]
    os.makedirs(user_applications_dir, exist_ok=True)
    user_desktop_dir = QtCore.QStandardPaths.standardLocations(
        QtCore.QStandardPaths.DesktopLocation)[0]
    os.makedirs(user_desktop_dir, exist_ok=True)
    user_autostart_dir = os.path.join(user_config_dir, "autostart")
    os.makedirs(user_autostart_dir, exist_ok=True)

    read_icon_path = matc.shared.get_app_icon_path()
    write_icon_path = matc.shared.get_config_path(matc.shared.APPLICATION_ICON_FILE_NAME)
    print(f"Copying {read_icon_path} to {write_icon_path}")
    shutil.copy(read_icon_path, write_icon_path)

    template_desktop_file_path: str = matc.shared.get_res_path(LINUX_SHORTCUT_TEMPLATE_FILE_NAME)
    with open(template_desktop_file_path, "r") as f:
        template_content_str = f.read()
    template = Template(template_content_str)
    exec_path = os.path.join(user_home_dir, ".local", "bin", matc.shared.APPLICATION_NAME)
    output_desktop_file_contents: str = template.substitute(exec=exec_path, icon=write_icon_path)
    with tempfile.TemporaryDirectory() as tmp_dir_path:
        print(f"{tmp_dir_path=}")
        output_desktop_file_path = os.path.join(tmp_dir_path, LINUX_SHORTCUT_FILE_NAME)

        with open(output_desktop_file_path, "w+") as output_file:
            output_file.write(output_desktop_file_contents)

        os.chmod(output_desktop_file_path, 0o744)
        print(f"Copying {output_desktop_file_path} to {user_applications_dir}")
        shutil.copy2(output_desktop_file_path, user_applications_dir)
        print(f"Copying {output_desktop_file_path} to {user_autostart_dir}")
        shutil.copy2(output_desktop_file_path, user_autostart_dir)
        print(f"Copying {output_desktop_file_path} to {user_desktop_dir}")
        shutil.copy2(output_desktop_file_path, user_desktop_dir)


def do_extra_setup():
    """
    Called from two different places: 1. setup.py 2. When first starting the application
    :return:
    """
    platform = matc.shared.get_platform()
    if platform == matc.shared.Platform.gnu_linux:
        do_extra_setup_gnu_linux()
    elif platform == matc.shared.Platform.windows:
        do_extra_setup_windows()
    elif platform == matc.shared.Platform.macos:
        do_extra_setup_macos()
    else:
        pass


def do_extra_setup_windows():
    """
    Two approaches:
    * Option A: Creating a windows shortcut file (.lnk file) and adding it to the startup dir and
    start menu.
      This is complicated to do since .lnk files are designed to be difficult to create. They are
      (at least look like)
      binary files. It is possible though if we install (using pip) pywin32 and winshell. But at
      the time of writing i don't think it's worth it since it would add two new depenedencies
      and increase the size of the from_pyinstaller_bundle package
    * Option B: Adding a registry entry for the application
      This requires admin priveliges

    Workaround: During the first startup of the application, we can ask the user to add the
    application to autostart
    and add a shortcut
    """
    logging.debug("Extra setup is not supported on Windows")


def do_extra_setup_macos():
    # TODO: Looking into this (need to talk to someone with a Mac computer)
    logging.debug("Extra setup is not supported on MacOS")
