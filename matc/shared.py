import enum
import logging
import os
import sys
import urllib.request
from string import Template

from PySide6 import QtCore

"""
This file contains
* constants
* enums
* global functions, for example:
  * relating to file names/paths (some using QtCore)
  * relating to font (some using QtGui)
"""

APPLICATION_PRETTY_NAME = "Mindfulness at the Computer"
APPLICATION_NAME = "mindfulness-at-the-computer"
EMAIL_ADDRESS = "tord.dellsen@gmail.com"
SHORT_DESCR = "Helps you stay mindful of your breathing while using your computer."
LOG_FILE_NAME_STR = "matc.log"

APPLICATION_ICON_FILE_NAME = "icon.png"
APPLICATION_ICON_WITH_DOT_FILE_NAME = "icon-b.png"

USER_FILES_DIR = "user_files"
IMAGES_DIR = "images"
ICONS_DIR = "icons"
AUDIO_DIR = "audio"
RES_DIR = "res"

LIGHT_GREEN_COLOR = "#bfef7f"
DARK_GREEN_COLOR = "#7fcc19"
DARKER_GREEN_COLOR = "#548811"
WHITE_COLOR = "#ffffff"
BLACK_COLOR = "#1C1C1C"


class BrVis(enum.Enum):
    bar = 0
    circle = 1
    line = 2
    columns = 3


class Platform(enum.Enum):
    gnu_linux = enum.auto()
    windows = enum.auto()
    macos = enum.auto()
    other = enum.auto()


class ApplRunMethod(enum.Enum):
    from_source = enum.auto()
    from_pyinstaller_bundle = enum.auto()


def get_platform() -> Platform:
    """
    Alternatively we can do this with Python's sysconfig:
    https://docs.python.org/3/library/sysconfig.html
    :return:
    """
    kernel_type: str = QtCore.QSysInfo.kernelType()
    if "linux" in kernel_type.lower():
        return Platform.gnu_linux
    if "darwin" in kernel_type.lower():
        return Platform.macos
    if "win" in kernel_type.lower():
        return Platform.windows
    return Platform.other


def get_appl_run_method() -> ApplRunMethod:
    # https://pyinstaller.org/en/stable/runtime-information.html
    if getattr(sys, "frozen", False):
        if hasattr(sys, "_MEIPASS"):
            return ApplRunMethod.from_pyinstaller_bundle
    return ApplRunMethod.from_source


def get_system_info() -> dict:
    sys_info_telist = {}
    sys_info_telist["Application name"] = APPLICATION_NAME
    sys_info_telist["Application version"] = get_version()
    sys_info_telist["Config path"] = get_config_path()
    sys_info_telist["Module path"] = get_module_path()
    sys_info_telist["Python version"] = sys.version
    sys_info_telist["Qt version"] = QtCore.qVersion()
    plugins_path: str = QtCore.QLibraryInfo.path(QtCore.QLibraryInfo.PluginsPath)
    sys_info_telist["Plugins path"] = plugins_path
    qt_sys_info = QtCore.QSysInfo()
    sys_info_telist["OS name and version"] = qt_sys_info.prettyProductName()
    kernel_type_and_version = qt_sys_info.kernelType() + " " + qt_sys_info.kernelVersion()
    sys_info_telist["Kernel type and version"] = kernel_type_and_version
    sys_info_telist["buildCpuArchitecture"] = qt_sys_info.buildCpuArchitecture()
    sys_info_telist["currentCpuArchitecture"] = qt_sys_info.currentCpuArchitecture()
    system_locale = QtCore.QLocale.system().name()
    sys_info_telist["System Localization"] = system_locale
    sys_info_telist["Application run method"] = get_appl_run_method().name
    """
    Other things we may want to add:
    self.tray_icon.isSystemTrayAvailable():
    self.tray_icon.supportsMessages():
    desktop_widget = matc_qapplication.desktop()
    desktop_widget.isVirtualDesktop()
    desktop_widget.screenCount()
    desktop_widget.primaryScreen()
    sys_info_telist.append(("Pyside version"))
    """

    return sys_info_telist


def get_config_path(*args) -> str:
    # application_dir_str = os.path.dirname(os.path.dirname(__file__))
    config_dir = QtCore.QStandardPaths.standardLocations(QtCore.QStandardPaths.ConfigLocation)[0]
    # logging.debug("QStandardPaths.ConfigLocation = " + config_dir)

    # There is a bug in Qt: For Windows, the application name is included in
    # QStandardPaths.ConfigLocation (for Linux, it's not included)
    if APPLICATION_NAME not in config_dir:
        config_dir = os.path.join(config_dir, APPLICATION_NAME)
    full_path_str = config_dir
    for arg in args:
        full_path_str = os.path.join(full_path_str, arg)
    os.makedirs(os.path.dirname(full_path_str), exist_ok=True)
    return full_path_str


def get_module_path(i_file_name: str = "") -> str:
    ret_module_dir_str: str = os.path.dirname(os.path.abspath(__file__))
    # -__file__ is the file that was started
    # base_dir_str: str = os.path.dirname(ret_module_dir_str)
    # base_dir_str = os.getcwd()
    if i_file_name:
        ret_module_dir_str = os.path.join(ret_module_dir_str, i_file_name)

    return ret_module_dir_str


def get_res_path(i_file_name: str = "") -> str:
    ret_res_path_str = os.path.join(get_module_path(), RES_DIR)
    if i_file_name:
        ret_res_path_str = os.path.join(ret_res_path_str, i_file_name)
    return ret_res_path_str


def get_audio_path(i_file_name: str = "") -> str:
    ret_audio_path_str = os.path.join(get_res_path(), AUDIO_DIR)
    if i_file_name:
        ret_audio_path_str = os.path.join(ret_audio_path_str, i_file_name)
    return ret_audio_path_str


def get_icon_path(i_file_name: str) -> str:
    ret_icon_path_str = os.path.join(get_res_path(), ICONS_DIR, i_file_name)
    return ret_icon_path_str


def get_app_icon_path(i_notification_dot: bool = False) -> str:
    if i_notification_dot:
        file_name = APPLICATION_ICON_WITH_DOT_FILE_NAME
    else:
        file_name = APPLICATION_ICON_FILE_NAME
    ret_icon_path_str = get_icon_path(file_name)
    return ret_icon_path_str


def get_html(i_text: str, i_focus: bool = False, i_margin: int = 0) -> str:
    html_template_base = (
        '<p style="text-align:center;padding:0px;margin:${margin}px;font-size:18px;${bold};'
        'color:${color_hex};">${text}</p>')
    html_template = Template(html_template_base)
    bold_html = ""
    color_hex: str = DARKER_GREEN_COLOR
    if i_focus:
        bold_html = "font-weight:bold;"
        color_hex: str = LIGHT_GREEN_COLOR
    ret_html = html_template.substitute(margin=i_margin, bold=bold_html, text=i_text,
        color_hex=color_hex)
    return ret_html


def get_version() -> str:
    version_file_path = get_module_path("version.txt")
    with open(version_file_path) as file:
        contents = file.read()
    ret_version = contents.strip()
    return ret_version


FAILED_TO_READ_REMOTE_VERSION = "failed"


def get_remote_version() -> str:
    url = ("https://gitlab.com/mindfulness-at-the-computer/mindfulness-at-the-computer.gitlab.io"
           "/-/raw/master/application-version.txt")
    try:
        with urllib.request.urlopen(url) as latest_version_source:
            remote_version_contents = latest_version_source.read()
    except urllib.error.URLError:
        logging.warning(
            "Could not download remote version file. This may simply be because you are "
            "temporarily disconnected from the internet."
        )
        return FAILED_TO_READ_REMOTE_VERSION
    remote_version_contents = remote_version_contents.decode('utf-8')
    ret_remote_version = remote_version_contents.strip()
    return ret_remote_version


def is_update_available() -> bool:
    local_version = get_version()
    latest_version = get_remote_version()
    if latest_version == FAILED_TO_READ_REMOTE_VERSION:
        return False
    if local_version == latest_version:
        return False
    return True
